$(function () {
    // Create the chart
    $('#prductos_mes').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Ventas Del Mes'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total percent market share'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Pizza',
                y: 56.33,
                drilldown: 'Pizza'
            }, {
                name: 'Pescado',
                y: 24.03,
                drilldown: 'Pescado'
            }, {
                name: 'Pasta',
                y: 10.38,
                drilldown: 'Pasta'
            }, {
                name: 'Sopas',
                y: 4.77,
                drilldown: 'Sopas'
            }, {
                name: 'Bebidas',
                y: 0.91,
                drilldown: 'Bebidas'
            }, {
                name: 'Otros Productos',
                y: 0.2,
                drilldown: null
            }]
        }],
        drilldown: {
            series: [{
                name: 'Pizza',
                id: 'Pizza',
                data: [
                    [
                        'Queso y pepperoni',
                        24.13
                    ],
                    [
                        'Jamón y palmitos',
                        17.2
                    ],
                    [
                        'Margherita',
                        8.11
                    ],
                    [
                        'Hawaiana',
                        5.33
                    ],
                    [
                        'Pollo',
                        1.06
                    ],
                    [
                        'Jamón y panceta',
                        0.5
                    ]
                ]
            }, {
                name: 'Pescado',
                id: 'Pescado',
                data: [
                    [
                        'Plato Pescado',
                        5
                    ],
                    [
                        'Plato Pescado2',
                        4.32
                    ],
                    [
                        'Plato Pescado3',
                        3.68
                    ],
                    [
                        'Plato Pescado4',
                        2.96
                    ],
                    [
                        'Plato Pescado5',
                        2.53
                    ],
                    [
                        'Plato Pescado6',
                        1.45
                    ],
                    [
                        'Plato Pescado7',
                        1.24
                    ],
                    [
                        'Plato Pescado8',
                        0.85
                    ],
                    [
                        'Plato Pescado9',
                        0.6
                    ],
                    [
                        'Plato Pescado10',
                        0.55
                    ],
                    [
                        'Plato Pescado11',
                        0.38
                    ],
                    [
                        'Plato Pescado12',
                        0.19
                    ],
                    [
                        'Plato Pescado13',
                        0.14
                    ],
                    [
                        'Plato Pescado14',
                        0.14
                    ]
                ]
            }, {
                name: 'Pasta',
                id: 'Pasta',
                data: [
                    [
                        'Plato de pasta',
                        2.76
                    ],
                    [
                        'Plato de pasta2',
                        2.32
                    ],
                    [
                        'Plato de pasta3',
                        2.31
                    ],
                    [
                        'Plato de pasta4',
                        1.27
                    ],
                    [
                        'Plato de pasta5',
                        1.02
                    ],
                    [
                        'Plato de pasta6',
                        0.33
                    ],
                    [
                        'Plato de pasta7',
                        0.22
                    ],
                    [
                        'Plato de pasta8',
                        0.15
                    ]
                ]
            }, {
                name: 'Sopas',
                id: 'Sopas',
                data: [
                    [
                        'Sopas 1',
                        2.56
                    ],
                    [
                        'Sopas 2',
                        0.77
                    ],
                    [
                        'Sopas 3',
                        0.42
                    ],
                    [
                        'Sopas 4',
                        0.3
                    ],
                    [
                        'Sopas 5',
                        0.29
                    ],
                    [
                        'Sopas 6',
                        0.26
                    ],
                    [
                        'Sopas 7',
                        0.17
                    ]
                ]
            }, {
                name: 'Bebidas',
                id: 'Bebidas',
                data: [
                    [
                        'Pesi',
                        0.34
                    ],
                    [
                        'coca cola',
                        0.24
                    ],
                    [
                        'chinoto',
                        0.17
                    ],
                    [
                        'cafe',
                        0.16
                    ]
                ]
            }]
        }
    });
    $('#prductos_dia').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Ventas Del Dia'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total percent market share'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Pizza',
                y: 56.33,
                drilldown: 'Pizza'
            }, {
                name: 'Pescado',
                y: 24.03,
                drilldown: 'Pescado'
            }, {
                name: 'Pasta',
                y: 10.38,
                drilldown: 'Pasta'
            }, {
                name: 'Sopas',
                y: 4.77,
                drilldown: 'Sopas'
            }, {
                name: 'Bebidas',
                y: 0.91,
                drilldown: 'Bebidas'
            }, {
                name: 'Otros Productos',
                y: 0.2,
                drilldown: null
            }]
        }],
        drilldown: {
            series: [{
                name: 'Pizza',
                id: 'Pizza',
                data: [
                    [
                        'Queso y pepperoni',
                        24.13
                    ],
                    [
                        'Jamón y palmitos',
                        17.2
                    ],
                    [
                        'Margherita',
                        8.11
                    ],
                    [
                        'Hawaiana',
                        5.33
                    ],
                    [
                        'Pollo',
                        1.06
                    ],
                    [
                        'Jamón y panceta',
                        0.5
                    ]
                ]
            }, {
                name: 'Pescado',
                id: 'Pescado',
                data: [
                    [
                        'Plato Pescado',
                        5
                    ],
                    [
                        'Plato Pescado2',
                        4.32
                    ],
                    [
                        'Plato Pescado3',
                        3.68
                    ],
                    [
                        'Plato Pescado4',
                        2.96
                    ],
                    [
                        'Plato Pescado5',
                        2.53
                    ],
                    [
                        'Plato Pescado6',
                        1.45
                    ],
                    [
                        'Plato Pescado7',
                        1.24
                    ],
                    [
                        'Plato Pescado8',
                        0.85
                    ],
                    [
                        'Plato Pescado9',
                        0.6
                    ],
                    [
                        'Plato Pescado10',
                        0.55
                    ],
                    [
                        'Plato Pescado11',
                        0.38
                    ],
                    [
                        'Plato Pescado12',
                        0.19
                    ],
                    [
                        'Plato Pescado13',
                        0.14
                    ],
                    [
                        'Plato Pescado14',
                        0.14
                    ]
                ]
            }, {
                name: 'Pasta',
                id: 'Pasta',
                data: [
                    [
                        'Plato de pasta',
                        2.76
                    ],
                    [
                        'Plato de pasta2',
                        2.32
                    ],
                    [
                        'Plato de pasta3',
                        2.31
                    ],
                    [
                        'Plato de pasta4',
                        1.27
                    ],
                    [
                        'Plato de pasta5',
                        1.02
                    ],
                    [
                        'Plato de pasta6',
                        0.33
                    ],
                    [
                        'Plato de pasta7',
                        0.22
                    ],
                    [
                        'Plato de pasta8',
                        0.15
                    ]
                ]
            }, {
                name: 'Sopas',
                id: 'Sopas',
                data: [
                    [
                        'Sopas 1',
                        2.56
                    ],
                    [
                        'Sopas 2',
                        0.77
                    ],
                    [
                        'Sopas 3',
                        0.42
                    ],
                    [
                        'Sopas 4',
                        0.3
                    ],
                    [
                        'Sopas 5',
                        0.29
                    ],
                    [
                        'Sopas 6',
                        0.26
                    ],
                    [
                        'Sopas 7',
                        0.17
                    ]
                ]
            }, {
                name: 'Bebidas',
                id: 'Bebidas',
                data: [
                    [
                        'Pesi',
                        0.34
                    ],
                    [
                        'coca cola',
                        0.24
                    ],
                    [
                        'chinoto',
                        0.17
                    ],
                    [
                        'cafe',
                        0.16
                    ]
                ]
            }]
        }
    });
    $('#prductos_years').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Ventas Del Año'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total percent market share'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Pizza',
                y: 56.33,
                drilldown: 'Pizza'
            }, {
                name: 'Pescado',
                y: 24.03,
                drilldown: 'Pescado'
            }, {
                name: 'Pasta',
                y: 10.38,
                drilldown: 'Pasta'
            }, {
                name: 'Sopas',
                y: 4.77,
                drilldown: 'Sopas'
            }, {
                name: 'Bebidas',
                y: 0.91,
                drilldown: 'Bebidas'
            }, {
                name: 'Otros Productos',
                y: 0.2,
                drilldown: null
            }]
        }],
        drilldown: {
            series: [{
                name: 'Pizza',
                id: 'Pizza',
                data: [
                    [
                        'Queso y pepperoni',
                        24.13
                    ],
                    [
                        'Jamón y palmitos',
                        17.2
                    ],
                    [
                        'Margherita',
                        8.11
                    ],
                    [
                        'Hawaiana',
                        5.33
                    ],
                    [
                        'Pollo',
                        1.06
                    ],
                    [
                        'Jamón y panceta',
                        0.5
                    ]
                ]
            }, {
                name: 'Pescado',
                id: 'Pescado',
                data: [
                    [
                        'Plato Pescado',
                        5
                    ],
                    [
                        'Plato Pescado2',
                        4.32
                    ],
                    [
                        'Plato Pescado3',
                        3.68
                    ],
                    [
                        'Plato Pescado4',
                        2.96
                    ],
                    [
                        'Plato Pescado5',
                        2.53
                    ],
                    [
                        'Plato Pescado6',
                        1.45
                    ],
                    [
                        'Plato Pescado7',
                        1.24
                    ],
                    [
                        'Plato Pescado8',
                        0.85
                    ],
                    [
                        'Plato Pescado9',
                        0.6
                    ],
                    [
                        'Plato Pescado10',
                        0.55
                    ],
                    [
                        'Plato Pescado11',
                        0.38
                    ],
                    [
                        'Plato Pescado12',
                        0.19
                    ],
                    [
                        'Plato Pescado13',
                        0.14
                    ],
                    [
                        'Plato Pescado14',
                        0.14
                    ]
                ]
            }, {
                name: 'Pasta',
                id: 'Pasta',
                data: [
                    [
                        'Plato de pasta',
                        2.76
                    ],
                    [
                        'Plato de pasta2',
                        2.32
                    ],
                    [
                        'Plato de pasta3',
                        2.31
                    ],
                    [
                        'Plato de pasta4',
                        1.27
                    ],
                    [
                        'Plato de pasta5',
                        1.02
                    ],
                    [
                        'Plato de pasta6',
                        0.33
                    ],
                    [
                        'Plato de pasta7',
                        0.22
                    ],
                    [
                        'Plato de pasta8',
                        0.15
                    ]
                ]
            }, {
                name: 'Sopas',
                id: 'Sopas',
                data: [
                    [
                        'Sopas 1',
                        2.56
                    ],
                    [
                        'Sopas 2',
                        0.77
                    ],
                    [
                        'Sopas 3',
                        0.42
                    ],
                    [
                        'Sopas 4',
                        0.3
                    ],
                    [
                        'Sopas 5',
                        0.29
                    ],
                    [
                        'Sopas 6',
                        0.26
                    ],
                    [
                        'Sopas 7',
                        0.17
                    ]
                ]
            }, {
                name: 'Bebidas',
                id: 'Bebidas',
                data: [
                    [
                        'Pesi',
                        0.34
                    ],
                    [
                        'coca cola',
                        0.24
                    ],
                    [
                        'chinoto',
                        0.17
                    ],
                    [
                        'cafe',
                        0.16
                    ]
                ]
            }]
        }
    });
});