var cards = [
            {
                "id": 1,
                "local_id": 1,
                "platillo_id": 11,
                "existencia": 44,
                "created": "2016-09-10T02:38:08+00:00",
                "modified": "2016-09-10T02:38:08+00:00",
                "platillo": {
                    "id": 11,
                    "photo": "1hUdr-OU7MY.jpg",
                    "dir": "webroot\\files\\Platillos\\photo\\",
                    "nombre": "licores 1",
                    "descripcion": "Descripcion de los licores",
                    "precio": 1000,
                    "especialidad_id": 1,
                    "created": "2016-09-06T08:09:33+00:00",
                    "modified": "2016-09-06T08:09:33+00:00",
                    "ingredienteplatillos": []
                }
            },
            {
                "id": 2,
                "local_id": 1,
                "platillo_id": 11,
                "existencia": 45,
                "created": "2016-09-13T04:11:15+00:00",
                "modified": "2016-09-13T04:11:15+00:00",
                "platillo": {
                    "id": 11,
                    "photo": "1hUdr-OU7MY.jpg",
                    "dir": "webroot\\files\\Platillos\\photo\\",
                    "nombre": "licores 1",
                    "descripcion": "Descripcion de los licores",
                    "precio": 1000,
                    "especialidad_id": 1,
                    "created": "2016-09-06T08:09:33+00:00",
                    "modified": "2016-09-06T08:09:33+00:00",
                    "ingredienteplatillos": []
                }
            }
        ]
var menu = angular.module('mymenu', ["angularUtils.directives.dirPagination"]);
menu.controller('card', function($scope) {
    $scope.cards = cards
    $scope.server = server;
    if (typeof(localStorage["car"])=="undefined") {
        $scope.car = [];
        localStorage["car"] = "[]";
      }else {
        $scope.car = JSON.parse(localStorage["car"]);
        angular.forEach($scope.cards, function(value, key) {
            angular.forEach($scope.car, function(value2, key2) {
                if (value2.id==value.id) {
                    value.in_card = true;
                    value.class = "success";
                    value.cantidad = value2.cantidad;
                }
            });
        });
      }
      if (typeof(localStorage["franquisia"])=="undefined") {
        $scope.is_selet_franquisia = false;
      }else {
        $scope.is_selet_franquisia = true;
      }
    $scope.add_card = function(producto) {
        if (producto.cantidad>0) {
            $scope.car.push(producto);
            producto.in_card = true;
            producto.class = "success";
            localStorage["car"] = JSON.stringify($scope.car);
        }else{
            alert("Debes Ordenar mas de un plato");
        }
    }
    $scope.remove_card = function(producto) {
        $scope.car = $scope.car.filter(function(item) {
            return item.id != producto.id
        })
        producto.in_card = false;
        setTimeout(function() {
            producto.class = "warning";
        }, 100)
        localStorage["car"] = JSON.stringify($scope.car);
    }
    $scope.update_card = function(producto, type) {
        angular.forEach($scope.car, function(value, key) {
          if (producto.id==value.id) {
            if (type=="up") {
              value.cantidad = value.cantidad+1;
            } else if (type=="down" && value.cantidad > 1) {
              value.cantidad = value.cantidad-1;
            }
          }
        });
        angular.forEach($scope.cards, function(value, key) {
          if (producto.id==value.id) {
            if (type=="up") {
              value.cantidad = value.cantidad+1;
            } else if (type=="down" && value.cantidad > 1) {
              value.cantidad = value.cantidad-1;
            }
          }
        });
        localStorage["car"] = JSON.stringify($scope.car);
    }
    $scope.focus_delete = function(producto) {
        producto.class = "danger";
    }
    $scope.blur_delete = function(producto) {
        if (producto.in_card) {
            producto.class = "success";
        }else{
            producto.class = "warning";
        }
    }
    $scope.views_card = function(producto) {
        $scope.platillo = producto
    }
    $scope.vaciar_car = function() {
        angular.forEach($scope.cards, function(value, key) {
          value.class = "warning";
          value.in_card = false;
        });
        $scope.car = []
        localStorage["car"] = "[]";
    }
});
angular.bootstrap(document.getElementById("App2"), ['mymenu']);
