var car = angular.module('mycar', ["angularUtils.directives.dirPagination"]);

car.controller('car', function($scope) {
    if (typeof(localStorage["car"])=="undefined") {
      $scope.car = [];
      localStorage["car"] = "[]";
    }else {
      $scope.car = JSON.parse(localStorage["car"]);
    }
    if (typeof(localStorage["franquisia"])=="undefined") {
      $scope.is_selet_franquisia = false;
    }else {
      $scope.is_selet_franquisia = true;
    }
    function calculate() {
      $scope.subtotal = 0
        $scope.subtotal = 0
        $scope.tax = 0
        $scope.descuento = 0
        $scope.total = 0
      angular.forEach($scope.car, function(value, key) {
        $scope.subtotal = $scope.subtotal + (value.cantidad*value.costo)
        $scope.tax = ($scope.subtotal*10)/100
        $scope.total = $scope.subtotal+$scope.tax
      });
    }
    calculate()
    $scope.remove_card = function(producto) {
        $scope.car = $scope.car.filter(function(item) {
            return item.id != producto.id
        })
        producto.in_card = false;
        setTimeout(function() {
            producto.class = "warning";
        }, 100)
        localStorage["car"] = JSON.stringify($scope.car);
        calculate()
    }
    $scope.update_card = function(producto, type) {
        angular.forEach($scope.car, function(value, key) {
          if (producto.id==value.id) {
            if (type=="up") {
              value.cantidad = value.cantidad+1;
            } else if (type=="down" && value.cantidad > 1) {
              value.cantidad = value.cantidad-1;
            }
          }
        });
        localStorage["car"] = JSON.stringify($scope.car);
        calculate()
    }
    $scope.focus_delete = function(producto) {
        producto.class = "danger";
    }
    $scope.blur_delete = function(producto) {
        if (producto.in_card) {
            producto.class = "success";
        }else{
            producto.class = "warning";
        }
    }
    $scope.views_card = function(producto) {
        $scope.platillo = producto
    }
    $scope.vaciar_car = function() {
        angular.forEach($scope.cards, function(value, key) {
          value.class = "warning";
          value.in_card = false;
        });
        $scope.car = []
        localStorage["car"] = "[]";
        calculate()
        
    }
});

angular.bootstrap(document.getElementById("App2"), ['mycar']);
