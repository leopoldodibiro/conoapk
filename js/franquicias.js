
var menu = angular.module('mymenu', ["angularUtils.directives.dirPagination"]);
menu.controller('card', function($scope, $http) {
    $scope.server = server;
    $scope.franquisias = [];
    $scope.select_franquisia = function(producto) {
        localStorage["franquisia"] = JSON.stringify(producto);
        window.location.replace("menu.html");
    }
    $scope.views_franquisia = function(producto) {
        $scope.franquisia = producto
    }
    function setHeader(xhr) {
      xhr.setRequestHeader('Authorization', token);
    }
    $http({
      method: 'GET',
      url: server+'api/locals',
    }).then(function (data) {
      $scope.franquisias = data.data.data;
      console.log($scope.franquisias)
    }, function (data) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
});
angular.bootstrap(document.getElementById("App2"), ['mymenu']);
