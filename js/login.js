var user = [
  {
    id: 1,
    nombre: 'leopoldo',
    apellido: 'montesinos',
    telefono: '0414-4434371',
    email: 'leopoldodibiro@gmail.com',
    username: 'dibiro',
    password: '123',
    role: 'administrador',
    active: true,
  },
]
var menu = angular.module('mylogin', ["angularUtils.directives.dirPagination"]);
menu.controller('login', function($scope) {
    $scope.users = user
    localStorage["user"] = "undefined";
    if (typeof(localStorage["users"])=="undefined") {
        localStorage["users"] = JSON.stringify($scope.users);
      }else {
        $scope.users = JSON.parse(localStorage["users"]);
      }
      if (typeof(localStorage["franquisia"])=="undefined") {
        $scope.is_selet_franquisia = false;
      }else {
        $scope.is_selet_franquisia = true;
      }
    $scope.add_user = function(user) {
      if (user.password==user.password2) {
        $.ajax({
          url: server+'api/user/register',
          type: 'POST',
          dataType: 'json',
          data: {
            'username': user.email,
            'password': user.password,
            'lastname': user.apellido,
            'firstname': user.nombre,
            'email': user.email,
            'phone': user.telefono,
          },
        })
        .done(function(data) {
          location.reload()
        })
        .fail(function(data) {
        })
        .always(function(data) {
        });
      }else {
        setTimeout(function() {alert('Password No Coiciden');}, 1000);
      }
    }
    $scope.login = function(user) {
      $.ajax({
        url: server+'api/auth',
        type: 'POST',
        dataType: 'json',
        data: {
          'password': user.password,
          'email': user.email,
        },
      })
      .done(function(data) {
        console.log(data)
        var decoded = jwt_decode(data.token, {complete:true});
        console.log(decoded);
      })
      .fail(function(data) {
      })
      .always(function(data) {
      });
    }
    
});
angular.bootstrap(document.getElementById("App2"), ['mylogin']);
