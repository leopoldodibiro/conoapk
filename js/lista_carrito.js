var cars = angular.module('mycars', ["angularUtils.directives.dirPagination"]);

cars.controller('cars', function($scope) {
    if (typeof(localStorage["car"])=="undefined") {
      $scope.car = [];
      localStorage["car"] = "[]";
    }else {
      $scope.car = JSON.parse(localStorage["car"]);
    }
    if (typeof(localStorage["franquisia"])=="undefined") {
      $scope.is_selet_franquisia = false;
    }else {
      $scope.is_selet_franquisia = true;
    }
});
angular.bootstrap(document.getElementById("App2"), ['mycars']);
