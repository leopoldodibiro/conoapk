var cabzera = angular.module('barras', ["angularUtils.directives.dirPagination"]);
cabzera.controller('barra', function($scope) {
    setInterval(function() {
      if (typeof(localStorage["car"])=="undefined") {
        $scope.car = [];
        localStorage["car"] = "[]";
      }else {
        $scope.car = JSON.parse(localStorage["car"]);
        $scope.carlength = $scope.car.length;
        $scope.$apply();
      }
    }, 1000);
    if (typeof(localStorage["franquisia"])=="undefined") {
      $scope.is_selet_franquisia = false;
    }else {
      $scope.franquisia = JSON.parse(localStorage["franquisia"]);
      $scope.is_selet_franquisia = true;
    }
    if (localStorage["user"]=="undefined") {
      $scope.is_authenticated = false;
    }else {
      $scope.user = JSON.parse(localStorage["user"]);
      $scope.is_authenticated = true;
    }
}).directive('leopoldoNav', function() {
  return {
    restrict: 'E',
    templateUrl: 'js/navbar.html'
  };
});