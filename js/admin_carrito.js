

var cards = [
    {
        id: 1,
        title: "perros calientes",
        costo: 10.56,
        description: "loren insop loren insop loren insoploren insoplorenvloren insop loren insop",
        img: "img/perro.jpg",
        class: "warning",
        in_card: false,
        cantidad: 1
    },
    {
        id: 2,
        title: "pizza",
        costo: 15.76,
        description: "loren insop loren insop loren insoploren insoplorenvloren insop loren insop",
        img: "img/pizza.jpg",
        class: "warning",
        in_card: false,
        cantidad: 1
    },
    {
        id: 3,
        title: "mango",
        costo: 2.50,
        description: "loren insop loren insop loren insoploren insoplorenvloren insop loren insop",
        img: "img/mango.jpg",
        class: "warning",
        in_card: false,
        cantidad: 1
    },
    {
        id: 4,
        title: "pasta",
        costo: 9.56,
        description: "loren insop loren insop loren insoploren insoplorenvloren insop loren insop",
        img: "img/pasta.jpg",
        class: "warning",
        in_card: false,
        cantidad: 1
    },
    {
        id: 5,
        title: "tomatero",
        costo: 18.56,
        description: "loren insop loren insop loren insoploren insoplorenvloren insop loren insop",
        img: "img/tomatero.jpg",
        class: "warning",
        in_card: false,
        cantidad: 1
    },
    {
        id: 6,
        title: "pepitona",
        costo: 69.56,
        description: "loren insop loren insop loren insoploren insoplorenvloren insop loren insop",
        img: "img/pepitona.jpg",
        class: "warning",
        in_card: false,
        cantidad: 1
    },
    {
        id: 7,
        title: "pescado",
        costo: 6.56,
        description: "loren insop loren insop loren insoploren insoplorenvloren insop loren insop",
        img: "img/pescado.jpg",
        class: "warning",
        in_card: false,
        cantidad: 1
    },
]

$('#cliente').select2({
  placeholder: 'Selecione Un Cliente',
  language: {
       noResults: function(){
           return "Cliente No Econtrado";
       }
   }
});

var menu = angular.module('mycar', ["angularUtils.directives.dirPagination"]);
menu.controller('car', function($scope) {
    $scope.cards = cards
    if (typeof(localStorage["users"])=="undefined") {
        localStorage["users"] = JSON.stringify($scope.users);
      }else {
        $scope.users = JSON.parse(localStorage["users"]);
        $("#cliente").append($('<option>'));
        $.each($scope.users, function(index, val) {
          $("#cliente").append($('<option>', { 
            value: val.id,
            text : val.nombre+' '+val.apellido 
          }));
        });
      }
    if (typeof(localStorage["car"])=="undefined") {
      $scope.car = [];
      localStorage["car"] = "[]";
    }else {
      $scope.car = JSON.parse(localStorage["car"]);
      angular.forEach($scope.cards, function(value, key) {
          angular.forEach($scope.car, function(value2, key2) {
              if (value2.id==value.id) {
                  value.in_card = true;
                  value.class = "success";
                  value.cantidad = value2.cantidad;
              }
          });
      });
    }
    if (typeof(localStorage["franquisia"])=="undefined") {
      $scope.is_selet_franquisia = false;
    }else {
      $scope.is_selet_franquisia = true;
    }
    $scope.Descuento = 0
    $scope.calculate = function() {
      $scope.subtotal = 0
      $scope.tax = 0
      $scope.descuento = 0
      $scope.total = 0
      angular.forEach($scope.car, function(value, key) {
        $scope.subtotal = parseFloat($scope.subtotal + (value.cantidad*value.costo))
        $scope.tax = parseFloat(($scope.subtotal*10)/100)
        $scope.descuento = parseFloat(($scope.subtotal*$scope.Descuento)/100)
        $scope.total = parseFloat($scope.subtotal+$scope.tax-$scope.descuento)
      });
    }
    $scope.calculate()
    $scope.add_card = function(producto) {
        if (producto.cantidad>0) {
            $scope.car.push(producto);
            producto.in_card = true;
            producto.class = "success";
            localStorage["car"] = JSON.stringify($scope.car);
        }else{
            alert("Debes Ordenar mas de un plato");
        }
      $scope.calculate()
    }
    $scope.remove_card = function(producto) {
        $scope.car = $scope.car.filter(function(item) {
            return item.id != producto.id
        })
        producto.in_card = false;
        setTimeout(function() {
            producto.class = "warning";
        }, 100)
        localStorage["car"] = JSON.stringify($scope.car);
      $scope.calculate()

    }
    $scope.update_card = function(producto, type) {
        angular.forEach($scope.car, function(value, key) {
          if (producto.id==value.id) {
            if (type=="up") {
              value.cantidad = value.cantidad+1;
            } else if (type=="down" && value.cantidad > 1) {
              value.cantidad = value.cantidad-1;
            }
          }
        });
        angular.forEach($scope.cards, function(value, key) {
          if (producto.id==value.id) {
            if (type=="up") {
              value.cantidad = value.cantidad+1;
            } else if (type=="down" && value.cantidad > 1) {
              value.cantidad = value.cantidad-1;
            }
          }
        });
        localStorage["car"] = JSON.stringify($scope.car);
      $scope.calculate()

    }
    $scope.focus_delete = function(producto) {
        producto.class = "danger";
    }
    $scope.blur_delete = function(producto) {
        if (producto.in_card) {
            producto.class = "success";
        }else{
            producto.class = "warning";
        }
    }
    $scope.views_card = function(producto) {
        $scope.platillo = producto
    }
    $scope.vaciar_car = function() {
      angular.forEach($scope.cards, function(value, key) {
        value.class = "warning";
        value.in_card = false;
      });
      $scope.car = []
      localStorage["car"] = "[]";
      $scope.calculate()

    }
});

angular.bootstrap(document.getElementById("App2"), ['mycar']);


